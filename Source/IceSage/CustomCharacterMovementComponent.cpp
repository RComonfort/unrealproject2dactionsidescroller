// Fill out your copyright notice in the Description page of Project Settings.

#include "CustomCharacterMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Character.h"
#include "IceSageStatics.h"


UCustomCharacterMovementComponent::UCustomCharacterMovementComponent (const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer) {}


void UCustomCharacterMovementComponent::TickComponent (float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent (DeltaTime, TickType, ThisTickFunction);

	//If in process of clambering
	if (bIsClambering)
	{
		
		float sqrDist = FVector::DistSquared (GetActorLocation (), clamberTarget);
		//If close to clamber target, stop
		if (sqrDist < ClamberSuccessSqrThreshold)
		{
			bIsClambering = false;
		}
		else
		{
			//GetOwner ()->SetActorLocation (clamberTarget, false, nullptr, ETeleportType::TeleportPhysics);
			FVector newPos = FMath::Lerp<FVector> (GetActorLocation (), clamberTarget, clamberSpeed * DeltaTime);

			UE_LOG (LogTemp, Warning, TEXT ("%s"), *newPos.ToString ());
			GetOwner ()->SetActorLocation(newPos);
		}
	}
}

bool UCustomCharacterMovementComponent::TryClamber()
{
	//If character wants to jump, check first if clamber possible
    AActor* legActor = nullptr,* waistActor = nullptr, *chestActor = nullptr;
	DoTraceInFrontOfChar (&legActor, &waistActor, &chestActor);

    if (!chestActor && (waistActor || legActor) && !bIsClambering)
    {
		AActor* climbActor = waistActor ? waistActor : legActor;
        Clamber(climbActor);
		return true;
    }
    else
        return false; 
}
void UCustomCharacterMovementComponent::Clamber (AActor* clamberObj)
{
	//Get bounds of clamber obj
	FVector outCenter, outExtents;
	clamberObj->GetActorBounds (false, outCenter, outExtents);

	//Get half height of character' capsule
	float charHeight = Cast<UCapsuleComponent> (GetCharacterOwner ()->GetRootComponent ())->GetScaledCapsuleHalfHeight ();

	FVector cornerPoint = outCenter + FVector(outExtents.X * (UIceSageStatics::IsActorLookingRight (GetOwner ()) ? -1 : 1), 0.0f, outExtents.Z);

	clamberTarget = cornerPoint + FVector::UpVector * charHeight;
	clamberSpeed = FVector::Dist(GetOwner()->GetActorLocation(), clamberTarget) / ClamberTime;
	bIsClambering = true;
}

void UCustomCharacterMovementComponent::DoTraceInFrontOfChar (AActor** legLevelActor, AActor** waistLevelActor, AActor** chestLevelActor)
{
	USkeletalMeshComponent* mesh = CharacterOwner->GetMesh ();
	ensure (mesh);

	FVector legPos = mesh->GetSocketTransform (LegLevelSocketName).GetLocation();
	FVector waistPos = mesh->GetSocketTransform (WaistLevelSocketName).GetLocation ();
	FVector chestPos = mesh->GetSocketTransform (ChestLevelSocketName).GetLocation();

	FVector endOffset = GetOwner ()->GetActorForwardVector () * ClamberCheckDist;

	UWorld* world = GetWorld ();
	const FName TraceTag ("ClamberCheck");
	world->DebugDrawTraceTag = TraceTag;

	FCollisionQueryParams CollisionParams;
	CollisionParams.TraceTag = TraceTag;
	FHitResult legTrace, waistTrace, chestTrace;

	bool legRes, waistRes, chestRes;
	legRes = world->LineTraceSingleByChannel (legTrace, legPos, legPos + endOffset, ECC_Visibility, CollisionParams);
	waistRes = world->LineTraceSingleByChannel (waistTrace, waistPos, waistPos + endOffset, ECC_Visibility, CollisionParams);
	chestRes = world->LineTraceSingleByChannel (chestTrace, chestPos, chestPos + endOffset, ECC_Visibility, CollisionParams);

	if (legRes)
		*legLevelActor = legTrace.Actor.Get ();

	if (waistRes)
		*waistLevelActor = waistTrace.Actor.Get ();

	if (chestRes)
		*chestLevelActor = chestTrace.Actor.Get ();
	
}
