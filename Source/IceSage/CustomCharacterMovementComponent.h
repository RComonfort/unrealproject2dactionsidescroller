// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CustomCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class ICESAGE_API UCustomCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY ()

#pragma region Clamber

private:

	FVector clamberTarget;
	float clamberSpeed;

	void DoTraceInFrontOfChar (AActor** outLegResult, AActor** outWaistResult, AActor** outChestResult);


public:

	UPROPERTY (BlueprintReadOnly, Category = "Clamber")
	bool bIsClambering = false;

	UPROPERTY (EditAnywhere, BlueprintReadonly, Category = "Clamber")
		FName ChestLevelSocketName;

	UPROPERTY (EditAnywhere, BlueprintReadonly, Category = "Clamber")
		FName WaistLevelSocketName;

	UPROPERTY (EditAnywhere, BlueprintReadonly, Category = "Clamber")
		FName LegLevelSocketName;

	//Distance that the character is able to check for clamber
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Clamber")
		float ClamberCheckDist = 0.5f;

	//Distance to clamber point needed for it to succeed
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Clamber")
		float ClamberSuccessSqrThreshold = 25.0f;

	//Seconds needed for the clamber to complete
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Clamber")
		float ClamberTime = 1.5f;

	void Clamber (AActor* clamberObj);

	bool TryClamber ();

#pragma endregion


#pragma region Overrides

public:
	UCustomCharacterMovementComponent (const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get ());


	virtual void TickComponent (float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;


#pragma endregion

};
