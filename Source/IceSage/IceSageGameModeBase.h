// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "IceSageGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ICESAGE_API AIceSageGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
