// Fill out your copyright notice in the Description page of Project Settings.

#include "IceSageStatics.h"

// Sets default values
bool UIceSageStatics::IsActorLookingRight (AActor* actor)
{
	if (actor)
		return actor->GetActorForwardVector ().X > 0.0f;
	return false;
}

