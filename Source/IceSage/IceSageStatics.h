// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "IceSageStatics.generated.h"


UCLASS()
class ICESAGE_API UIceSageStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	

public:	
	static bool IsActorLookingRight (AActor* actor);

};
