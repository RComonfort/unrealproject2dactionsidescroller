// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"
#include "Components/InputComponent.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "CustomCharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"


APlayerCharacter::APlayerCharacter (const FObjectInitializer& ObjectInitializer):
	Super (ObjectInitializer.SetDefaultSubobjectClass<UCustomCharacterMovementComponent> (ACharacter::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent> (TEXT ("SpringArm"));
	SpringArmComp->AttachTo (RootComponent);

	CamComp = CreateDefaultSubobject<UCameraComponent> (TEXT ("Camera"));
	CamComp->AttachTo (SpringArmComp);

	ReticleWidget = CreateDefaultSubobject<UWidgetComponent> (TEXT ("ReticleWidget"));
	ReticleWidget->AttachTo (RootComponent);

	SCharacterMovComp = Cast<UCustomCharacterMovementComponent> (GetMovementComponent ());
}

// Sets default values
/*APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent> (TEXT ("SpringArmComp"));
	SpringArmComp->AttachTo (RootComponent);


	CamComp = CreateDefaultSubobject<UCameraComponent> (TEXT ("CameraComp"));
	CamComp->AttachTo (SpringArmComp);
}*/

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ReticleLogicTick (DeltaTime);
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis ("AddXReticleMovement", this, &APlayerCharacter::AddXReticleMovement);
	PlayerInputComponent->BindAxis ("AddYReticleMovement", this, &APlayerCharacter::AddYReticleMovement);
	PlayerInputComponent->BindAxis ("MoveRight", this, &APlayerCharacter::MoveRight);

	PlayerInputComponent->BindAction ("Jump", EInputEvent::IE_Pressed, this, &APlayerCharacter::JumpAction);
	PlayerInputComponent->BindAction ("Freeze", EInputEvent::IE_Pressed, this, &APlayerCharacter::BeginFreeze);
	PlayerInputComponent->BindAction ("Freeze", EInputEvent::IE_Released, this, &APlayerCharacter::EndFreeze);
}

void APlayerCharacter::JumpAction ()
{
	bool beganClamber = SCharacterMovComp->TryClamber ();

	if (!beganClamber)
		ACharacter::Jump ();
}


bool APlayerCharacter::IsClambering ()
{
	return SCharacterMovComp->bIsClambering;
}

void APlayerCharacter::ReticleLogicTick (float DeltaTime)
{
	//put the reticle at its radius
	ReticleWidget->AddLocalOffset (ReticleNextFrameOffset);
	FVector reticleDir = ReticleWidget->RelativeLocation.GetSafeNormal ();
	ReticleWidget->SetRelativeLocation (reticleDir * ReticleRadius);

	//Update control rotation so character looks at reticle
	FRotator lookAt = UKismetMathLibrary::FindLookAtRotation (GetActorLocation (), ReticleWidget->GetComponentLocation ());
	FaceRotation (lookAt, DeltaTime);

	//Reset offset
	ReticleNextFrameOffset = FVector::ZeroVector;
}

void APlayerCharacter::AddXReticleMovement (float value)
{
	ReticleNextFrameOffset.X += value;
}

void APlayerCharacter::AddYReticleMovement (float value)
{
	ReticleNextFrameOffset.Z += value;
}

void APlayerCharacter::MoveRight (float value)
{
	AddMovementInput (GetActorForwardVector (), value);
}

void APlayerCharacter::BeginFreeze ()
{

}

void APlayerCharacter::EndFreeze ()
{

}

void APlayerCharacter::Block ()
{

}

