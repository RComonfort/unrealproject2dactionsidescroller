// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UCustomCharacterMovementComponent;
class UWidgetComponent;

UCLASS()
class ICESAGE_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	//APlayerCharacter();

	APlayerCharacter (const class FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UCustomCharacterMovementComponent* SCharacterMovComp;

public:	

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = PlayerTweak)
	float ReticleRadius = 1.0f;

	UPROPERTY (VisibleAnywhere, Category = PlayerCharacter)
	UCameraComponent* CamComp;

	UPROPERTY(VisibleAnywhere, Category = PlayerCharacter)
	USpringArmComponent* SpringArmComp;

	UPROPERTY (VisibleAnywhere, Category = PlayerCharacter)
	UWidgetComponent* ReticleWidget;

	UFUNCTION()
	void AddXReticleMovement (float value);
	UFUNCTION ()
	void AddYReticleMovement (float value);
	UFUNCTION ()
	void MoveRight (float value);
	UFUNCTION()
	void JumpAction ();

	UFUNCTION ()
	void BeginFreeze ();
	UFUNCTION ()
	void EndFreeze ();
	UFUNCTION ()
	void Block ();

	UFUNCTION(BlueprintCallable, Category = "PlayerCharacter")
	bool IsClambering();

private:

	void ReticleLogicTick (float DeltaTime);

	FVector ReticleNextFrameOffset;

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
